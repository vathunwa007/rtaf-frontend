import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux";
import { Navbar } from '../../components';
import PropTypes from 'prop-types';
import Axios from 'axios';
import Config from '../../config';
import styled from 'styled-components';
import imglogo from '../../assets/img/Logo_RTAF.png';
import imguser from '../../assets/img/Icon_username.png';
import imgkey from '../../assets/img/Icon_Password.png';
import Swal from 'sweetalert2'
import { loadReCaptcha, ReCaptcha } from 'react-recaptcha-v3';
import jwt_decode from "jwt-decode";
import {
    loginData
} from "../../redux/actions";
const InputGroup = styled.div`
    display: -ms-flexbox; /* IE10 */
  display: flex;
  img{
      padding-bottom:2%;
  }
`;
const Footer = styled.div`
    position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  color: white;
  text-align: end;
`;


function Login() {
    const dispatch = useDispatch();
    const reduxLogin = useSelector(state => state.loginData);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    useEffect(() => {
        loadReCaptcha(Config.configReCaptchaKey, () => {
            console.log("Recapcha vertifly")

        });

    }, []);

    const login = () => {

        Swal.fire({
            title: 'กำลังล็อกอินเข้าสู่ระบบ!',
            type: 'success',
            timer: 100000,
            showConfirmButton: false,
            timerProgressBar: true,
            onBeforeOpen: () => {
                Swal.showLoading();

            }
        }).then((result) => {
            if (result.dismiss === Swal.DismissReason.timer) {
                Swal.fire({
                    confirmButtonText: 'ตกลง!',
                    icon: 'error',
                    title: 'เข้าสู่ระบบผิดพลาด...',
                    text: 'การเข้าสู่ระบบผิดพลาดหรือใช้เวลานาเกินไป กรุณาลองใหม่อีกครั้ง!',
                })
            }
        })
        Axios({
            method: "post",
            url: Config.API_URL + "/users/login",
            /* headers: {
                NAME_SCHEMA: this.props.provinceMode.NAME_SCHEMA_MODE,
            }, */
            data: {
                user_name: username,
                password: password
            },
            config: { headers: { "Content-Type": "multipart/form-data" } },
        })
            .then(async (res) => {
                console.log(res.data)
                var user = jwt_decode(res.data.access_token);
                console.log(user)
                user.token = res.data.access_token;
                window.sessionStorage.setItem("token", JSON.stringify(res.data));
                window.sessionStorage.setItem("isLogin", 1);
                await dispatch(loginData(user));
                //await console.log(reduxLogin)
                //dispatch(loginData({reduxLogin, token:res.data.access_token}));

                let timerInterval;
                Swal.fire({
                    title: 'เข้าสู่ระบบสำเร็จ!',
                    type: 'success',
                    timer: 2000,
                    showConfirmButton: false,
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                            const content = Swal.getContent()
                            if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                    b.textContent = Swal.getTimerLeft()
                                }
                            }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                        window.sessionStorage.setItem("isLogin", 1);
                        window.location.href = "/home";
                    }
                })
            }).catch((eror) => {
                Swal.fire({
                    showConfirmButton: true,
                    icon: 'error',
                    title: 'เข้าสู่ระบบผิดพลาด...',
                    text: 'การเข้าสู่ระบบผิดพลาดหรือใช้เวลานานเกินไป กรุณาลองใหม่อีกครั้ง!',
                })
               /*  window.sessionStorage.setItem("isLogin", 0);
                setUsername("");
                setPassword(""); */
                window.sessionStorage.setItem("isLogin", 1);
                window.location.href = "/home";
            });
    }

    return (
        <div className="loginclass">
            <div className="CLlogin">
                <div className="wrapper">
                    <div className="container">
                        <img src={imglogo} height="300" alt="" />
                        <h1>ระบบบริหารจัดการ กทค.ฟสท.สอ.ทอ</h1>
                        <h3 className="text-light">Telecom Managegement System</h3>
                        <form className="form" onSubmit={
                            (e) => {
                                /**
                                 * Prevent submit from reloading the page
                                 */
                                e.preventDefault();
                                e.stopPropagation();
                                login();
                            }
                        }>
                            <InputGroup>
                                {/* <i className="fas fa-user icon" /> */}
                                <img src={imguser} alt="" width="50" />
                                <input type="text" placeholder="Username" value={username} onChange={
                                    (e) => {
                                        setUsername(e.target.value);
                                    }
                                } />
                            </InputGroup>
                            <InputGroup>
                                {/* <i className="fas fa-key icon" /> */}
                                <img src={imgkey} alt="" width="50" />
                                <input type="password" placeholder="Password" value={password} onChange={
                                    (e) => {
                                        setPassword(e.target.value);
                                    }
                                } />
                            </InputGroup>

                            <button type="submit" className="btn btn-warning" onClick={() => {
                                login();
                            }}>เข้าสู่ระบบ</button>
                            <button type="button" className="btn btn-primary">ยกเลิก</button>
                        </form>
                    </div>

                </div>

            </div>
            <Footer>สอบถามรายละเอียดติดต่อ 2-3313</Footer>
        </div>
    );
}

export default Login;