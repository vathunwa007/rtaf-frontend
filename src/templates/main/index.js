import { Navbar, Leftbar, Headers } from '../../components';
import { pageDashboard, pageHome, pageRtafdetail, pageReportdanger, pageStatus, pageTelcom, pageDischargebatterry, pageManage } from '../../page';
import { Addreportdanger,Addrtafdetail } from '../../page/adddatapage';

import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import $ from "jquery";
import { Layout, Menu, Breadcrumb, } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;


const Homepage = () => {
    const [leftmenu, setleftmenu] = useState([]);
    function onCollapse(collapsed) {
        console.log(collapsed);
    };
    return (
        <div className="Homepage">
            <Layout>
                <Headers></Headers>
                <Layout style={{ minHeight: '100vh' }}>
                    <Leftbar></Leftbar>
                    <Layout className="site-layout">
                        <Content style={{ margin: '0 16px' }}>{/*swich page ต่างๆจาก route ที่ตรงนี้*/}

                            <Switch>
                                {/* หน้าเพิ่มข้อมูล */}
                                <Route path={`${process.env.PUBLIC_URL}/addreportdanger`} component={Addreportdanger} />    {/*หน้าเพิ่มรายงานเหตุการณ์ประจำวัน*/}
                                <Route path={`${process.env.PUBLIC_URL}/addrtafdetail`} component={Addrtafdetail} />    {/*หน้าเพิ่มสถานี*/}




                                {/* หน้าหลัก */}
                                <Route exact path={`${process.env.PUBLIC_URL}/dashboard`} component={pageDashboard} />   {/*หน้าแดชบอร์ด*/}
                                <Route path={`${process.env.PUBLIC_URL}/home`} component={pageHome} />                   {/*หน้าแรก*/}
                                <Route path={`${process.env.PUBLIC_URL}/rtafdetail`} component={pageRtafdetail} />       {/*ข้อมูลสถานี*/}
                                <Route path={`${process.env.PUBLIC_URL}/reportdanger`} component={pageReportdanger} />   {/*รายงานเหตุการณ์ประจำวัน*/}
                                <Route path={`${process.env.PUBLIC_URL}/status`} component={pageStatus} />               {/*สถานะการทำงานของอุปกรณ์*/}
                                <Route path={`${process.env.PUBLIC_URL}/telcom`} component={pageTelcom} />               {/*ระบบสื่อสารโทรคมนาคม*/}
                                <Route path={`${process.env.PUBLIC_URL}/dischargebatterry`} component={pageDischargebatterry} />    {/*Dischargebatterry*/}
                                <Route path={`${process.env.PUBLIC_URL}/manage`} component={pageManage} />    {/*Dischargebatterry*/}
                                <Route path={`${process.env.PUBLIC_URL}/`} component={pageHome} />                       {/*หน้าแรก*/}

                            </Switch>


                        </Content>
                        <Footer style={{ textAlign: 'center' }}>RTAF ©2020 Created RTAF</Footer>
                    </Layout>
                </Layout>
            </Layout>

        </div>
    );
}

export default Homepage;
