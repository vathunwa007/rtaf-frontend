import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import PrivateRoute from './privateRoute'
import LoginTemplate from '../templates/login'
import MainTemplate from '../templates/main'
import {Addreportdanger} from '../page/adddatapage'

class RouteRoot extends Component {
  render () {
    return (
      <Router>
        <Switch>
          <Route exact path={`${process.env.PUBLIC_URL}/login`} component={LoginTemplate} />
          <Route exact path={`${process.env.PUBLIC_URL}/test`} component={Addreportdanger} />
          <PrivateRoute  path={`${process.env.PUBLIC_URL}/`} component={MainTemplate} />

        </Switch>
      </Router>
    )
  }
}

export default RouteRoot
