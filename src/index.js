import React from 'react'
import ReactDOM from 'react-dom'
import RouteRoot from './routes/rootRoute'
import * as serviceWorker from './serviceWorker'
import './assets/css/main.css';
import Config from './config';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import allReducer from './redux/reducers'
import 'antd/dist/antd.css';


//window.sessionStorage.setItem("isLogin", 1);
//window.sessionStorage.clear();
const store = createStore(
    allReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
ReactDOM.render(
    <Provider store={store}>
        <RouteRoot />
        {/* , document.getElementById('root') */}
</Provider>,
document.getElementById('root')

);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
