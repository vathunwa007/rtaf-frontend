import React, { useState, useEffect } from 'react';
import $ from "jquery";
import { useSelector, useDispatch } from "react-redux";
import {
    loginData
} from "../redux/actions";
import * as Getapi from '../utilities'


const Home = () => {
    const dispatch = useDispatch();
    const reduxLogin = useSelector(state => state.loginData)
    const [name, setName] = useState("thunwa");
    const [test, setTest] = useState("aaaa");

    useEffect(() => {
        //console.log(reduxLogin);

    }, [])
    return (
        <div className="">
            <div className="row mt-3">
                <div className="col-6 ">
                    <span style={{fontSize:"25px"}}>ศทค.ชั้น2.เขาเขียว(HGL)</span> <br/>
                    <span>ชื่อสถาณี:{test}</span><br/>
                    <span>รหัส:{name}</span><br/>
                    <span>โทรศัพท์ภายใน:{reduxLogin.isLogin}</span><br/>
                    <span>หมายเหตุ:</span>
                </div>
                <div className="col-6 ">
                    <div className="">
                        <img src="https://i2.wp.com/www.whatphone.net/wp-content/uploads/2015/10/ISS-and-Earth-open.jpg?fit=690%2C398&ssl=1" height="250" width="100%"alt="" />
                    </div>
                </div>
                <div className="col-6 ">
                    <span>รายชื่อเจ้าหน้าที่ประจำสถานี (จำนวน 25 คน)</span>
                    <table className="table">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">First</th>
                                <th scope="col">Last</th>
                                <th scope="col">Handle</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td>@twitter</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td>@twitter</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="col-6 ">
                    <span>ตำแหน่งสถานี:หินตั้งเมืองชลบุรี</span><br/>
                    <span>DMS:13.78137,100.5583647</span>
                    <iframe width="100%" height="250" frameborder="0"
                        src={`https://maps.google.com/maps?q=${13.78137}, ${100.5583647}&z=15&output=embed`}></iframe>

                </div>
            </div>
        </div>
    );
}

export default Home;
