import React, { useState, useEffect } from 'react';
import $ from "jquery";
import { useSelector, useDispatch } from "react-redux";
import { Table, Tag, Space, Breadcrumb, Input, Button ,DatePicker, Switch} from 'antd';
import { SearchOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { Addedittable } from '../components';

import {
    login
} from "../redux/actions";
const { RangePicker } = DatePicker;
const add=()=>{
    console.log("5555")
}
const edit=()=>{
    console.log("5555")
}
const deletes=()=>{
    console.log("5555")
}
const columns = [
    {
        title: 'ลำดับ',
        dataIndex: 'name',
        key: 'name',
        render: text => <a>{text}</a>,
    },
    {
        title: 'ปี',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'เดือน',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'สถานี',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <>
                {tags.map(tag => {
                    let color = tag.length > 5 ? 'geekblue' : 'green';
                    if (tag === 'loser') {
                        color = 'volcano';
                    }
                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </>
        ),
    },
    {
        title: 'จัดการ',
        key: 'action',
        render: (text, record) => (
            <Addedittable view={add} edit={edit} deletes={deletes}/>
         ),
    },
];

const data = [];
for (let i = 0; i < 10; i++) {
    data.push({
        key: i,
        name: `Edward King ${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
        tags: ["name"]
    });
}
function onChange(date, dateString) {
    console.log(date, dateString);
  }
const Telcom = () => {
    const dispatch = useDispatch();
    const reduxLogin = useSelector(state => state.loginData)
    const [table, setTable] = useState(data);
    const [state, setState] = useState({namestanee:"",year:"",month:""});


    return (
        <div className="C_Telcom">
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>สถานีโทรคมนาคม</Breadcrumb.Item>
                <Breadcrumb.Item>ระบบสื่อสารโทรคมนาคม</Breadcrumb.Item>
            </Breadcrumb>
            <Input
                placeholder="ค้นหาด้วยชื่อสถานี"
                enterButton="Search"
                size="large"
                onSearch={value => console.log(value)}
                style={{ width: "200px" }}
                onChange={(e)=>{setState({...state,namestanee:e.target.value})}}
            />
                   <DatePicker onChange={onChange} size="large" picker="year" placeholder="ปี" onChange={(e,year)=>{setState({...state,year:year})}}/>
                   <DatePicker onChange={onChange} format="MM" size="large" mode="month" picker="month" placeholder="เดือน" onChange={(e,month)=>{setState({...state,month:month})}}/>

            <Button icon={<SearchOutlined />} size="large">ค้นหา</Button>
            <Button icon={<PlusCircleOutlined />} size="large">เพิ่ม</Button>
            <hr/>
            <Table columns={columns} dataSource={table} scroll={{x:true}}/>
        </div>
    );
}

export default Telcom;
