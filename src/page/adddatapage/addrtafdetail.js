import React, { useState, useEffect } from 'react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { UploadOutlined } from '@ant-design/icons';
import {
    Form,
    Input,
    Row, Col, Card, Upload, Button, message
} from 'antd';
const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not validate email!',
        number: '${label} is not a validate number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};

const Addrtafdetail = () => {
    const [componentSize, setComponentSize] = useState('default');
    const [fileList, setFileList] = useState([]);
    const [fileList2, setFileList2] = useState([]);
    const onFormLayoutChange = ({ size }) => {
        setComponentSize(size);
    };
    const download1 = {
        listType: "picture",

        onChange({ file, fileList }) {
            let blob = new Blob([file], { type: 'image/png' });
            var url = URL.createObjectURL(blob);
            file.url = url;
            if (fileList.length > 1) {
                console.log("uploadได้แค่1รูป", fileList);
                let filenew = fileList.filter((item) =>
                    item.uid != file.uid
                );

                console.log(filenew)
                fileList = filenew;
            }

        },
        onRemove: file => {
            setFileList([]);
        },
        beforeUpload: file => {
            setFileList([file])
            return false;
        },

        fileList
    };
    const download2 = {
        listType: 'picture',

        onChange({ file, fileList }) {
            let blob = new Blob([file], { type: 'image/png' });
            var url = URL.createObjectURL(blob);
            file.url = url;
            if (fileList.length > 1) {
                console.log("uploadได้แค่1รูป", fileList);
                let filenew = fileList.filter((item) =>
                    item.uid != file.uid
                );

                fileList2 = filenew;
                console.log(filenew)

            }

        },
        onRemove: file => {
            setFileList2([]);
        },
        beforeUpload: file => {
            setFileList2([file])
            return false;
        },


        fileList2
    };
    return (
        <div className="C_Addrtafdetail " >

            <Card style={{ width: "100%" }} className="mt-3">
                <h4 style={{ textAlign: "center" }}>เพิ่มข้อมูลสถานี</h4>
                <Form
                    validateMessages={validateMessages}
                    layout="vertical"
                    initialValues={{ size: componentSize }}
                    onValuesChange={onFormLayoutChange}
                    size={"large"}
                >
                    <Form.Item
                        name={['user', 'name']}
                        label="ภาพสถานี"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Upload {...download1} >
                            <Button icon={<UploadOutlined />}> {fileList && fileList[0] && fileList[0].name ? fileList[0].name : 'Click to Upload'}</Button>
                        </Upload>
                    </Form.Item>
                    <Form.Item name={['user', 'introduction']} label="ภาพตำแหน่งสถานี" rules={[
                        {
                            required: true,
                        },
                    ]}>
                        <Upload {...download2} >
                            <Button icon={<UploadOutlined />}> {fileList2 && fileList2[0] && fileList2[0].name ? fileList2[0].name : 'Click to Upload'}</Button>
                        </Upload>
                    </Form.Item>
                    <Form.Item  >
                        <button className="btn btn-primary btn-block w-50 mx-auto" htmlType="submit">บันทึก</button>
                        <button className="btn btn-danger btn-block w-50 mx-auto">ยกเลิก</button>
                    </Form.Item>
                </Form>
            </Card>



        </div >
    );
}

export default Addrtafdetail;
