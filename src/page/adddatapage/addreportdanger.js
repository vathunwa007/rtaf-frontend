import React, { useState, useEffect } from 'react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import CKEditor from '@ckeditor/ckeditor5-react'
import {
    Form,
    Input,
    Button,
    Radio,
    Select,
    Cascader,
    DatePicker,
    InputNumber,
    TreeSelect,
    Switch,
    Row, Col, Card, Upload
} from 'antd';
const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not validate email!',
        number: '${label} is not a validate number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};

const Addreportdanger = () => {
    const [componentSize, setComponentSize] = useState('default');
    const onFormLayoutChange = ({ size }) => {
        setComponentSize(size);
    };
    return (
        <div className="C_addreportdanger ">

            <Card style={{ width: "100%" }} className="mt-3">
                <h4 style={{ textAlign: "center" }}>บันทึกเหตุการณ์ประจำวัน</h4>
                <Form
                    validateMessages={validateMessages}
                    layout="vertical"
                    initialValues={{ size: componentSize }}
                    onValuesChange={onFormLayoutChange}
                    size={"large"}
                >
                    <Form.Item
                        name={['user', 'name']}
                        label="หัวข้อรายงานเหตุการณ์ประจำวัน"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item name={['user', 'introduction']} label="รายละเอียดข้อมูล" rules={[
                        {
                            required: true,
                        },
                    ]}>
                        {/* <Input.TextArea style={{height:"200px"}} /> */}
                        <CKEditor
                            editor={ClassicEditor}
                            onInit={editor => {

                            }}
                            onChange={(e, data) => {
                                console.log(data)
                                /* console.log(t.getData()) */
                            }}
                            config={
                                {
                                    ckfinder: {
                                        uploadUrl: '/upload',
                                    },
                                    removePlugins: ['ImageUpload'],

                                }
                            }
                        />
                    </Form.Item>
                    <Form.Item  >
                        <button className="btn btn-primary btn-block w-50 mx-auto" htmlType="submit">บันทึก</button>
                        <button className="btn btn-danger btn-block w-50 mx-auto">ยกเลิก</button>
                    </Form.Item>
                </Form>
            </Card>



        </div>
    );
}

export default Addreportdanger;
