import React, { useState, useEffect } from 'react';
import $ from "jquery";
import { useSelector, useDispatch } from "react-redux";
import { Table, Tag, Space, Breadcrumb, Input, Button, DatePicker, Switch, Select } from 'antd';
import { SearchOutlined, PlusCircleOutlined, FilePdfOutlined} from '@ant-design/icons';
import { Addedittable } from '../components';

import {
    login
} from "../redux/actions";
const { RangePicker } = DatePicker;
const { Option } = Select;
const add=()=>{
    console.log("5555")
}
const edit=()=>{
    console.log("5555")
}
const deletes=()=>{
    console.log("5555")
}
const columns = [
    {
        title: 'ลำดับ',
        dataIndex: 'key',
        key: 'key',
        render: text => <a>{text}</a>,
    },
    {
        title: 'เลขงาน',
        dataIndex: 'age',
        key: 'age',
        sorter: (a, b) => a.age - b.age,
    },
    {
        title: 'รายการสั่ง',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'ประเภทของ',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'หน่วยรับผิดชอบ',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'วันที่สั่ง',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <>
                {tags.map(tag => {
                    let color = tag.length > 5 ? 'geekblue' : 'green';
                    if (tag === 'loser') {
                        color = 'volcano';
                    }
                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </>
        ),
    },
    {
        title: 'จัดการ',
        key: 'action',
       render: (text, record) => (
            <Addedittable view={add} edit={edit} deletes={deletes}/>
         ),
    },
];

const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i+1,
        name: `Edward King ${i}`,
        age: Math.floor(Math.random() * 100),
        address: `London, Park Lane no. ${i}`,
        tags: ["name"]
    });
}
function onChange(date, dateString) {
    console.log(date, dateString);
}
const Manage = () => {
    const dispatch = useDispatch();
    const reduxLogin = useSelector(state => state.loginData)
    const [table, setTable] = useState(data);
    const [state, setState] = useState({ numberwork: "", listwork: "", typework: "" ,agencywork:"",date:"",statusbacklog:true,statusnumber:true,statusclose:true});

    return (
        <div className="C_Manage">
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>สถานีโทรคมนาคม</Breadcrumb.Item>
                <Breadcrumb.Item>จัดการใบสั่งงาน</Breadcrumb.Item>
            </Breadcrumb>
            <Input
                placeholder="เลขงาน"
                enterButton="Search"
                size="large"
                onSearch={value => console.log(value)}
                style={{ width: "100px" }}
                type="number"
                onChange={(e) => { setState({ ...state, numberwork: e.target.value }) }}

            />
            <Input
                placeholder="รายการสั่งงาน"
                enterButton="Search"
                size="large"
                onSearch={value => console.log(value)}
                style={{ width: "auto" }}
                onChange={(e) => { setState({ ...state, listwork: e.target.value }) }}

            />
            <Select
                showSearch
                style={{ width: 200 }}
                placeholder="ประเภทของงาน"
                optionFilterProp="children"
                filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                size="large"
                onChange={(value) => { setState({ ...state, typework: value }) }}
            >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
            </Select>
            <Select
                showSearch
                style={{ width: 200 }}
                placeholder="หน่วยที่รับผิดชอบ"
                optionFilterProp="children"
                filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                size="large"
                onChange={(value) => { setState({ ...state, agencywork:value}) }}
            >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
            </Select>
            <RangePicker size="large" format="DD-MM-YYYY" placeholder={["วันที่เริ่ม", "วันที่สิ้นสุด"]} onChange={(moment, date) => { setState({ ...state, date: date }) }} />

            <Button icon={<SearchOutlined />} size="large">ค้นหา</Button>
            <Button icon={<PlusCircleOutlined />} size="large">เพิ่ม</Button>
            <Button icon={<FilePdfOutlined />} size="large">Export</Button>
            <br />
            <div style={{width:"400px"}} className="mx-auto">
                <button className="ant-btn ant-btn-lg "><Switch checkedChildren="งานค้าง" unCheckedChildren="งานค้าง" defaultChecked={state.statusbacklog} className="checkbox1" onChange={(status) => { setState({ ...state, statusbacklog: status }) }} /></button>
                <button className="ant-btn ant-btn-lg"><Switch checkedChildren="ยังไม่มีเลขงาน" unCheckedChildren="ยังไม่มีเลขงาน" defaultChecked={state.statusnumber} className="checkbox2" onChange={(status) => { setState({ ...state, statusnumber: status }) }}/></button>
                <button className="ant-btn ant-btn-lg"><Switch checkedChildren="ปิดเรียบร้อย" unCheckedChildren="ปิดเรียบร้อย" defaultChecked={state.statusclose} className="" onChange={(status) => { setState({ ...state, statusclose: status }) }}/></button>
            </div>

            <hr />
            <Table  rowClassName={(record, index) => record.age == 5||record.age == 10||record.age == 15 ? 'table-row-blue' : record.age > 22 ? 'table-row-yellow':'table-row-red'} columns={columns} dataSource={table} scroll={{x:true}} dataSource={table} />
        </div >
    );
}

export default Manage;
