import React, { useState, useEffect,useDebugName } from 'react';
import $ from "jquery";
import { useSelector, useDispatch } from "react-redux";
import { Table, Tag, Space, Breadcrumb, Input, Button } from 'antd';
import { SearchOutlined, PlusCircleOutlined } from '@ant-design/icons';
import {Addedittable} from '../components';
import {
    login
} from "../redux/actions";
import { Link } from 'react-router-dom';
const Search = Input.Search;
const add=()=>{
    console.log("5555")
}
const edit=()=>{
    console.log("5555")
}
const deletes=()=>{
    console.log("5555")
}
const columns = [
    {
        title: 'ลำดับ',
        dataIndex: 'name',
        key: 'name',
        render: text => <a>{text}</a>,
    },
    {
        title: 'ชื่อสถานี',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'ตัวย่อ',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'โทรศัพท์ภายใน',
        key: 'โทรศัพท์ภายใน',
        dataIndex: 'tags',
        render: tags => (
            <>
                {tags.map(tag => {
                    let color = tag.length > 5 ? 'geekblue' : 'green';
                    if (tag === 'loser') {
                        color = 'volcano';
                    }
                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </>
        ),
    },
    {
        title: 'ตำบล',
        key: 'action',
        render: (text, record) => (
            <Space size="middle">
                <a>Invite {record.name}</a>
                <a>Delete</a>
            </Space>
        ),
    },
    {
        title: 'อำเภอ',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'จังหวัด',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'Lattitude',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'Longtitude',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'หมายเหตุ',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'จัดการ',
        dataIndex: 'manage',
        key: 'จัดการ',
        render: (text, record) => (
            <Addedittable view={add} edit={edit} deletes={deletes}/>
         ),
    },
];

const data = [];
for (let i = 0; i < 15; i++) {
    data.push({
        key: i,
        name: `Edward King ${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
        tags: ["name"],

    });
}

const Rtafdetail = () => {
    const dispatch = useDispatch();
    const reduxLogin = useSelector(state => state.loginData)
    const [state, setState] = useState({namecut:"",namestanee:""});

    return (
        <div className="C_rtafdetail">
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>สถานีโทรคมนาคม</Breadcrumb.Item>
                <Breadcrumb.Item>ข้อมูลสถานี</Breadcrumb.Item>
            </Breadcrumb>
            <Input
                placeholder="ค้นหาด้วยชื่อสถานี"
                enterButton="Search"
                size="large"
                onSearch={value => console.log(value)}
                style={{ width: "200px" }}
                defaultValue={state.namestanee}
                onChange={(e)=>{setState({ ...state,namestanee:e.target.value});}}
            />
            <Input
                placeholder="ค้นหาด้วยตัวย่อ"
                enterButton="Search"
                size="large"
                onSearch={value => console.log(value)}
                style={{ width: "200px" }}
                defaultValue={state.namecut}
                onChange={(e)=>{setState({ ...state,namecut:e.target.value});}}

            />
            <Button icon={<SearchOutlined />} size="large">ค้นหา</Button>
            <Link to="addrtafdetail"><Button icon={<PlusCircleOutlined />} size="large">เพิ่ม</Button></Link>
            <hr/>
            <Table columns={columns} dataSource={data} scroll={{x:true}} />
        </div>
    );
}

export default Rtafdetail;
