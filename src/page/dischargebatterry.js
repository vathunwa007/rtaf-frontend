import React, { useState, useEffect } from 'react';
import $ from "jquery";
import { useSelector, useDispatch } from "react-redux";
import { Table, Tag, Space, Breadcrumb, Input, Button, DatePicker, Switch,Select } from 'antd';
import { SearchOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { Addedittable } from '../components';
import locale from 'antd/lib/date-picker/locale/th_TH';

import {
    login
} from "../redux/actions";
const {Option} = Select;
const { RangePicker } = DatePicker;
const add=()=>{
    console.log("5555")
}
const edit=()=>{
    console.log("5555")
}
const deletes=()=>{
    console.log("5555")
}
const columns = [
    {
        title: 'ลำดับ',
        dataIndex: 'name',
        key: 'name',
        render: text => <a>{text}</a>,
    },
    {
        title: 'วันที่ตรวจสอบ',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'ยี่ห้อ/รุ่นอุปกรณ์',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'สถานี',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <>
                {tags.map(tag => {
                    let color = tag.length > 5 ? 'geekblue' : 'green';
                    if (tag === 'loser') {
                        color = 'volcano';
                    }
                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </>
        ),
    },
    {
        title: 'วันที่ติดตั้งใช้งาน',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'จำนวนวัน',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'จำนวนBatterry',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'จัดการ',
        key: 'action',
        render: (text, record) => (
            <Addedittable view={add} edit={edit} deletes={deletes}/>
        ),
    },
];

const data = [];
for (let i = 0; i < 10; i++) {
    data.push({
        key: i,
        name: `Edward King ${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
        tags: ["name"]
    });
}
function onChange(date, dateString) {
    console.log(date, dateString);
}
const Dischargebatterry = () => {
    const dispatch = useDispatch();
    const reduxLogin = useSelector(state => state.loginData)
    const [table, setTable] = useState(data);
    const [state, setState] = useState({ namestanee: "", date: "", type: "" });

    return (
        <div className="C_Dischargebatterry">
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>สถานีโทรคมนาคม</Breadcrumb.Item>
                <Breadcrumb.Item>ผลการDischargeBatterry</Breadcrumb.Item>
            </Breadcrumb>
            <Input
                placeholder="ค้นหาด้วยชื่อสถานี"
                enterButton="Search"
                size="large"
                onSearch={value => console.log(value)}
                style={{ width: "200px" }}
                onChange={(e) => { setState({ ...state, namestanee: e.target.value }) }}
            />
            <RangePicker locale={locale}size="large"  format="DD-MM-YYYY" placeholder={["วันที่เริ่ม", "วันที่สิ้นสุด"]} onChange={(moment, date) => { setState({ ...state, date: date }) }} />
            <Select
                showSearch
                size="large"
                style={{ width: 200 }}
                placeholder="ยี่ห้อรุ่น/อุปกรณ์"
                optionFilterProp="children"
                filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                onChange={(value) => { setState({ ...state, type: value}) }}
            >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
            </Select>
            <Button icon={<SearchOutlined />} size="large">ค้นหา</Button>
            <Button icon={<PlusCircleOutlined />} size="large">เพิ่ม</Button>
            <hr />
            <Table columns={columns} dataSource={table} scroll={{ x: true }} />
        </div>
    );
}

export default Dischargebatterry;
