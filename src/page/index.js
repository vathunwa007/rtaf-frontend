export {default as pageDashboard} from '../page/dashboard';
export {default as pageHome} from '../page/home';
export {default as pageRtafdetail} from '../page/rtafdetail';
export {default as pageReportdanger} from '../page/reportdanger';
export {default as pageStatus} from '../page/status';
export {default as pageTelcom} from '../page/telcom';
export {default as pageDischargebatterry} from '../page/dischargebatterry';
export {default as pageManage} from '../page/manage';
