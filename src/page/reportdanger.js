import React, { useState, useEffect } from 'react';
import $ from "jquery";
import { useSelector, useDispatch } from "react-redux";
import { Table, Tag, Space, Breadcrumb, Input, Button, DatePicker, Select, Modal } from 'antd';
import { SearchOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { Addedittable } from '../components';
import { Link } from 'react-router-dom';
import {
    login
} from "../redux/actions";
const { RangePicker } = DatePicker;
const { Option } = Select;
const options = [{ value: 'gold' }, { value: 'lime' }, { value: 'green' }, { value: 'cyan' }];

const Reportdanger = () => {
    const dispatch = useDispatch();
    const reduxLogin = useSelector(state => state.loginData)
    const [state, setState] = useState({ namereport: "", namestanee: "", date: "" });
    const [modalview, setModalview] = useState({ show: false, name: "", age: "" });

    const view = async (datarow) => {
        console.log("view");
        console.log(datarow);
        setModalview({ ...modalview, show: true, name: datarow.name,age:datarow.age });
    }
    const edit = () => {
        console.log("5555")
    }
    const deletes = () => {
        console.log("5555")
    }

    const columns = [
        {
            title: 'ลำดับ',
            dataIndex: 'key',
            key: 'key',
            render: text => <a>{text}</a>,
        }, {
            title: 'วันที่',
            dataIndex: 'name',
            key: 'name',
            render: text => <a>{text}</a>,
        },
        {
            title: 'หัวข้อรายงานเหตุการประจำวัน',
            dataIndex: 'age',
            key: 'age',
        },

        {
            title: 'ผู้บันทึก',
            key: 'tags',
            dataIndex: 'tags',
            render: tags => (
                <>
                    {tags.map(tag => {
                        let color = tag.length > 5 ? 'geekblue' : 'green';
                        if (tag === 'loser') {
                            color = 'volcano';
                        }
                        return (
                            <Tag color={color} key={tag}>
                                {tag.toUpperCase()}
                            </Tag>
                        );
                    })}
                </>
            ),
        },
        {
            title: 'การจัดการ',
            key: 'action',
            dataIndex: 'tags',
            render: (text, record) => (
                <Addedittable view={() => { view(record) }} edit={edit} deletes={deletes} />
            ),
        },
    ];

    const data = [];
    for (let i = 0; i < 10; i++) {
        data.push({
            key: i,
            name: `Edward King ${i}`,
            age: 32,
            tags: ["name"],
            dataimage:"asdasdasdasdasdasdasd",
        });
    }
    function tagRender(props) {
        const { label, value, closable, onClose } = props;

        return (
            <Tag color={value} closable={closable} onClose={onClose} style={{ marginRight: 3 }}>
                {label}
            </Tag>
        );
    }
    return (
        <div className="C_reportdanger">
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>สถานีโทรคมนาคม</Breadcrumb.Item>
                <Breadcrumb.Item>รายงานเหตุการณ์ประจำวัน</Breadcrumb.Item>
            </Breadcrumb>
            <Input
                placeholder="ค้นหาด้วยชื่อรายงาน"
                enterButton="Search"
                size="large"
                onSearch={value => console.log(value)}
                style={{ width: "200px" }}
                onChange={(e) => { setState({ ...state, namereport: e.target.value }); }}

            />
            <Select
                mode="multiple"
                showArrow
                placeholder="ตัวย่อสถานี"
                tagRender={tagRender}
                size="large"
                style={{ width: "300px" }}
                options={options}
                maxTagCount={4}
                onChange={(value) => { setState({ ...state, namestanee: value }); }}

            />
            <RangePicker size="large" format="DD-MM-YYYY" placeholder={["วันที่เริ่ม", "วันที่สิ้นสุด"]} onChange={(moment, date) => { setState({ ...state, date: date }) }} />

            <Button icon={<SearchOutlined />} size="large">ค้นหา</Button>
            <Link to="addreportdanger"><Button icon={<PlusCircleOutlined />} size="large">เพิ่ม</Button></Link>
            <hr />
            <Table columns={columns} dataSource={data} scroll={{ x: true }} />


            {/*   เปิดการใช้งาน modal view */}
            <Modal
                title="รายงานเหตุการณ์ประจำวัน"
                centered
                visible={modalview.show}
                onOk={() => setModalview(false)}
                onCancel={() => setModalview(false)}
                width={1000}
                style={{textAlign:"center"}}
            >
                <p>{modalview.name}</p>
                <p>{modalview.age}</p>
            </Modal>
        </div>
    );
}

export default Reportdanger;
