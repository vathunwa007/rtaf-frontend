import React, { useState, useEffect } from 'react';
import $ from "jquery";
import { useSelector, useDispatch } from "react-redux";
import { Table, Tag, Space, Breadcrumb, Input, Button ,DatePicker, Switch} from 'antd';
import { SearchOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { Addedittable } from '../components';

import {
    login
} from "../redux/actions";
const { RangePicker } = DatePicker;
const add=()=>{
    console.log("5555")
}
const edit=()=>{
    console.log("5555")
}
const deletes=()=>{
    console.log("5555")
}
const columns = [
    {
        title: 'ลำดับ',
        dataIndex: 'name',
        key: 'name',
        render: text => <a>{text}</a>,
    },
    {
        title: 'วันที่',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'สถานี',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'สถานะการทำงานอุปกรณ์',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'Alarm',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <>
                {tags.map(tag => {
                    let color = tag.length > 5 ? 'geekblue' : 'green';
                    if (tag === 'loser') {
                        color = 'volcano';
                    }
                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </>
        ),
    },
    {
        title: 'จัดการ',
        key: 'action',
        render: (text, record) => (
            <Addedittable view={add} edit={edit} deletes={deletes}/>
         ),
    },
];

const data = [];
for (let i = 0; i < 10; i++) {
    data.push({
        key: i,
        name: `Edward King ${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
        tags: ["name"]
    });
}
const Status = () => {
    const dispatch = useDispatch();
    const reduxLogin = useSelector(state => state.loginData)
    const [state, setState] = useState({namestanee:"",date:"",status:"",alarm:""});

    return (
        <div className="C_Status">
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>สถานีโทรคมนาคม</Breadcrumb.Item>
                <Breadcrumb.Item>สถานะการทำงานของอุปกรณ์</Breadcrumb.Item>
            </Breadcrumb>
            <Input
                placeholder="ค้นหาด้วยชื่อสถานี"
                enterButton="Search"
                size="large"
                onSearch={value => console.log(value)}
                style={{ width: "200px" }}
                onChange={(e)=>{setState({...state,namestanee:e.target.value})}}
            />
            <RangePicker size="large"format="DD-MM-YYYY" placeholder={["วันที่เริ่ม", "วันที่สิ้นสุด"]} onChange={(moment,date)=>{setState({ ...state,date:date})}}/>
                <button className="ant-btn ant-btn-lg"><Switch checkedChildren="ปิดสถานะไม่ปกติ" unCheckedChildren="ดูสถานะไม่ปกติ" defaultChecked className="checkbox1" onChange={(e)=>{setState({...state,status:e})}} /></button>
                <button className="ant-btn ant-btn-lg"><Switch checkedChildren="ปิดมีAlarm" unCheckedChildren="เปิดมีAlarm" defaultChecked className="checkbox2" onChange={(e)=>{setState({...state,alarm:e})}} /></button>
            <Button icon={<SearchOutlined />} size="large">ค้นหา</Button>
            <Button icon={<PlusCircleOutlined />} size="large">เพิ่ม</Button>
            <hr/>

            <Table columns={columns} dataSource={data} scroll={{x:true}}/>
        </div>
    );
}

export default Status;
