import React, { useState, useEffect } from 'react';
import { Tooltip, Button, Divider } from 'antd';
import { EyeFilled, EditFilled, MinusCircleFilled } from '@ant-design/icons';
import PropTypes from 'prop-types';
const Addedittable = ({ view, edit, deletes }) => {
    useEffect(() => {

    }, [])
    return (
        <div>
            {view ?
                <Tooltip title="ดู" color={'blue'} >
                    <Button onClick={() => { view() }}><EyeFilled style={{ color: "blue" }} /></Button>
                </Tooltip> : ''
            }{edit ?
            <Tooltip title="แก้ไข" color={'green'}  >
                <Button onClick={() => { edit() }}><EditFilled style={{ color: "green" }} /></Button>
            </Tooltip>
            :''}
            {edit?
            <Tooltip title="ลบ" color={'red'} >
                <Button onClick={() => { deletes() }}><MinusCircleFilled style={{ color: "red" }} /></Button>
            </Tooltip>
            :''}
        </div>
    );
}
Addedittable.propTypes = {
    view: PropTypes.func.isRequired,
    edit: PropTypes.func.isRequired,
    deletes: PropTypes.func.isRequired,
}

export default Addedittable;
