import React, { useState, useEffect } from 'react';
import { Layout, Menu, Breadcrumb, Dropdown } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import Logout from './logout';
import {
    DesktopOutlined,
    PieChartOutlined,
    FileOutlined,
    TeamOutlined,
    DownOutlined
} from '@ant-design/icons';
import Swal from 'sweetalert2'

const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;

const Headers = () => {
    const [showlogout, setShowlogout] = useState(false)
    const logout = async () => {
        await setShowlogout(true)
        setShowlogout(false)

    }
    const menu = (
        <Menu>
            <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" onClick={() => { logout() }}>
                    ออกจากระบบ
            </a>
            </Menu.Item>
        </Menu>
    );
    return (
        <div className="CLheader ">
            { showlogout ? <Logout /> : null}
            {/*  <Header className="header" style={{ height: '100px' }}>
                    <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                    <Menu.Item key="1">nav 1</Menu.Item>

                </Menu>
                <div style={{ display: "table", height: "100%", fontSize: "18px" }}>
                    <span style={{ color: "#FFF", display: "table-cell", verticalAlign: "middle", lineHeight: "normal" }}>ระบบบริหารจัดการทั่วไป กองโทรคมนาคม กองทัพอากาศ</span>

                </div>
                <div>
                <span style={{color:"#FFF"}}>สถานี:ชื่อสถานี(กองโทรคมนาคม)</span>
                <span>logout</span>
                </div>


            </Header> */}
            <nav className="navbar navbar-expand-md navbar-light " style={{ backgroundColor: "#001529" }}>
                <div style={{ width: "300px", margin: "0 auto" }}>
                    <div className="logo" />

                </div>

                <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <span style={{ color: "#FFF", display: "table-cell", fontSize: "25px", verticalAlign: "middle", lineHeight: "normal", textAlign: "center" }}>ระบบบริหารจัดการทั่วไป กองโทรคมนาคม กองทัพอากาศ</span>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto" style={{textAlign:"center"}}>
                        {/* <li className="nav-item active">
                            <form className="form-inline my-2 my-lg-0">
                                <div style={{ color: "#FFF" }}>
                                    <p>สถานี:ชื่อสถานี(กองโทรคมนาคม)</p>
                                    <a style={{ float: "right", textDecoration: "underline" }} onClick={() => { logout() }}>logout</a>
                                </div>

                            </form>
                        </li> */}
                        <li>
                            <Dropdown overlay={menu}>
                                <a className="ant-dropdown-link text-light" style={{fontSize:"16px"}} onClick={e => e.preventDefault()}>
                                    สถานี:ชื่อสถานี(กองโทรคมนาคม)<DownOutlined />
                                </a>
                            </Dropdown>
                        </li>

                    </ul>

                </div>

            </nav>
        </div>
    );
}

export default Headers;
