import React, { useState, useEffect } from 'react';
import { Layout, Menu, Breadcrumb, } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import {
    DesktopOutlined,
    PieChartOutlined,
    FileOutlined,
    TeamOutlined,
} from '@ant-design/icons';
import Swal from 'sweetalert2'

const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;

const Logout = () => {
    useEffect(() => {
        Swal.fire({
            title: 'คุณต้องการออกจากระบบ?',
            text: "คุณแน่ใจว่าต้องการออกจากระบบหรือไม่!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ออกจากระบบ!',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                    {
                        icon: 'success',
                        title: 'ออกจากระบบสำเร็จ',
                        text: 'กำลังกลับสู่หน้าเข้าสู่ระบบ.',
                        showConfirmButton: false,
                        timer: 1500
                    }
                )
                window.sessionStorage.setItem("isLogin", 0);
                window.location.href = "/login";
            }
        })

    }, [])
    return (
        <div className="C_Logout ">

        </div>
    );
}

export default Logout;
