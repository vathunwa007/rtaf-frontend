import React, { useState, useEffect } from 'react';
import $ from "jquery";
import Container from '../../page/dashboard';
import { Layout, Menu, Breadcrumb, } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined, UserSwitchOutlined, RadarChartOutlined } from '@ant-design/icons';
import {
    DesktopOutlined,
    PieChartOutlined,
    FileOutlined,
    TeamOutlined,
    HomeOutlined,
    HddOutlined,
    ClusterOutlined
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;

function onCollapse(collapsed) {
    console.log(collapsed);
};

const Leftbar = () => {
    /* function setmenustart() {
        var pathArray = window.location.pathname.split('/');
        console.log(pathArray[1])
        if (pathArray[1] === "dashboard") {
            setleftmenu(['1'])
        } else if (pathArray[1] === "home") {
            setleftmenu(['2'])
        } else if (pathArray[1] === "rtafdetail") {
            setleftmenu(['3'])
        }
    } */
    const [leftmenu, setleftmenu] = useState(['2']);

    useEffect(() => {

    },[])

    return (

        <Sider breakpoint="lg"
            collapsedWidth="0"
            onBreakpoint={broken => {

                console.log(broken);
            }}
            onCollapse={(collapsed, type) => {
                console.log(collapsed, type);
            }}
            width="300"

        >
            <div className="logo" />
            <Menu theme="dark" defaultSelectedKeys={[]} mode="inline">
                <Menu.Item key="1" icon={<PieChartOutlined />}>
                    <Link to="/dashboard">Dashboard</Link>
                </Menu.Item>
                <Menu.Item key="2" icon={<HomeOutlined />}>
                    <Link to="/home">หน้าแรก</Link>
                </Menu.Item>
                <SubMenu key="sub1" icon={<RadarChartOutlined />} title="สถานีโทรคมนาคม ทอ.">
                    <Menu.Item key="3"><Link to="rtafdetail">ข้อมูลสถานี</Link></Menu.Item>
                    <Menu.Item key="4"><Link to="reportdanger">รายงานเหตุการณ์ประจำวัน</Link></Menu.Item>
                    <Menu.Item key="5"><Link to="status">สถาณะการทำงานของอุปกรณ์</Link></Menu.Item>
                    <Menu.Item key="6"><Link to="telcom">ระบบสื่อสารโทรคมนาคม</Link></Menu.Item>
                    <Menu.Item key="7"><Link to="dischargebatterry">ผลการ DischargeBatterry</Link></Menu.Item>
                    <Menu.Item key="8"><Link to="manage">จัดการใบสั่งงาน</Link></Menu.Item>
                </SubMenu>

                <SubMenu key="sub2" icon={<HddOutlined />} title="คลังพัสดุ">
                    <Menu.Item key="9"><Link to="dashboard">จัดการคลังพัสดุ</Link></Menu.Item>
                </SubMenu>
                <SubMenu key="sub3" icon={<UserSwitchOutlined />} title="กำลังพล">
                    <Menu.Item key="10"><Link to="dashboard">จัดการกำลังพล</Link></Menu.Item>
                </SubMenu>
                <Menu.Item key="11" icon={<DesktopOutlined />}>
                    <Link to="home">โครงการที่ดำเนินการ</Link>
                </Menu.Item>

                <Menu.Item key="12" icon={<ClusterOutlined />} ><Link to="dashboard">จัดการระบบ</Link></Menu.Item>
            </Menu>
        </Sider>

    );
}

export default Leftbar;
