let config = {
  // run production on interset server
  // configURL: 'http://interset.ddns.net:50000/backend',

  // run production on interset server
  API_URL:'http://localhost:4000',
  configURL: 'https://gis.nso.go.th/backend',

  configBasenameURL: '/backend',
  configReCaptchaKey: '6LeLW88ZAAAAAKheuRrrxUhGeX1PgyF-QJ98taob',
};

export default Object.freeze(Object.assign({}, config));
