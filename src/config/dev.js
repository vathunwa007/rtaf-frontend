let config = {
  API_URL:'http://localhost:4000',
  configURL: 'http://localhost/backend/public',

  configBasenameURL: '/backend/public',
  configReCaptchaKey: '6LeLW88ZAAAAAKheuRrrxUhGeX1PgyF-QJ98taob',
};

export default Object.freeze(Object.assign({}, config));
