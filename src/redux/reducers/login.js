const loginState = {
  user_name: "",
  first_name: "",
  last_name: "",
  e_mail: "",
  status: '',
  last_login: '',
  login_status: '',
  iat: '',
  exp: '',
  token: ''
}

// form of possible
// dispatch(login({username: '', token: '',}))
// dispatch(logout)

const loginReducer = (state = loginState, action) => {
  switch (action.type) {
    case "LOGIN":
      state = {
        user_name: action.payload.user_name ? action.payload.user_name : state.user_name,
        first_name: action.payload.first_name ? action.payload.first_name : state.first_name,
        last_name: action.payload.last_name ? action.payload.last_name : state.last_name,
        e_mail: action.payload.e_mail ? action.payload.e_mail : state.e_mail,
        status: action.payload.status ? action.payload.status : state.status,
        last_login: action.payload.last_login ? action.payload.last_login : state.last_login,
        login_status: 1,
        iat: action.payload.iat ? action.payload.iat : state.iat,
        exp: action.payload.exp ? action.payload.exp : state.exp,
        token: action.payload.token ? action.payload.token : state.token,
      }
      return state;
    case "LOGOUT":
      state = {
        user_name: "",
        first_name: "",
        last_name: "",
        e_mail: "",
        status: '',
        last_login: '',
        login_status: 0,
        iat: '',
        exp: '',
        token: '',
      }
      return state;
    default:
      return state;
  }
}

export default loginReducer;